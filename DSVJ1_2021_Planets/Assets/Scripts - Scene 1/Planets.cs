﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Planets : MonoBehaviour
{
   [Serializable]
   public class PlanetData
    {
        public float translationRadius;
        public float translationSpeed;
        public float size;

        public Vector3 rotationAxis;
        public float rotationSpeed;
        public Material mat;
    }

    public float speed = 5;
    public float radius = 2;
    public float angle = 0;

    public float rotationAngle = 0;

    public Vector3 wantedScale;

    public float rotationSpeed = 5;

    public Vector3 rotationDirection;

    public void Init(PlanetData pd, float size)
    {
        if (size < 1.0f)
        {
            size = 1;
        }

        radius = pd.translationRadius;
        speed = pd.translationSpeed * (size / 2);
        rotationDirection = pd.rotationAxis;
        rotationSpeed = pd.rotationSpeed * (size / 4);
        wantedScale = Vector3.one * transform.localScale.magnitude;
        GetComponent<MeshRenderer>().material = pd.mat;
    }

    

    private void Update()
    {
        Vector3 v3 = Vector3.zero;
        angle += speed * Time.deltaTime;
        v3.x = radius * Mathf.Cos(angle);
        v3.z = radius * Mathf.Sin(angle);

        transform.position = v3;

        transform.localScale = wantedScale;

        transform.Rotate(rotationDirection * rotationSpeed * Time.deltaTime);
    }
}
