﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class PlanetInstanciate : MonoBehaviour
{
    [SerializeField] Planets planetPrefab;
    [SerializeField] GameObject planetHolder;
    public List<Planets.PlanetData> planetList;
    List<Planets> planetGeneration = new List<Planets>();

    void Start()
    {
        for(short i=0;i<planetList.Count;i++)
        {
            Planets newPlanet = Instantiate<Planets>(planetPrefab);
            planetGeneration.Add(newPlanet);

            newPlanet.transform.parent = planetHolder.transform;
            float randomValue = UnityEngine.Random.Range(1, 4);
            Vector3 randomSize = new Vector3(randomValue, randomValue, randomValue);
            newPlanet.transform.localScale = randomSize;

            planetGeneration[i].Init(planetList[i], randomValue);
            planetGeneration[i].GetComponent<Transform>();
        }
    }
}
